<?php

namespace Drupal\domain_sites\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Defines Drush commands for the Domain Sites module.
 */
class DomainSitesCommands extends DrushCommands {

  /**
   * The domain storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $domainStorage;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $db;

  /**
   * Constructs a DomainSitesCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current active database's master connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection) {
    parent::__construct();

    $this->domainStorage = $entity_type_manager->getStorage('domain');
    $this->db = $connection;
  }

  /**
   * Assign nodes to the default domain.
   *
   * @command domain-sites:nodes
   *
   * @option force Force assigning the default domain.
   *
   * @usage drush domain-sites:nodes
   *   Assign nodes to the default domain.
   * @usage drush domain-sites:nodes --force
   *   Force assigning nodes to the default domain,
   *   even when they already have a domain assigned.
   *
   * @aliases domain-sites-nodes
   */
  public function assignNodes(array $options = ['force' => FALSE]) {
    $default_domain = $this->domainStorage->loadDefaultId();

    if (!$default_domain) {
      throw new \Exception(dt('No default domain found.'));
    }

    // Get all nodes that don't have a domain yet.
    $query = $this->db->select('node', 'n');
    $query->fields('n', ['type', 'nid', 'vid', 'langcode']);
    $query->fields('nfda', ['entity_id', 'field_domain_access_target_id']);
    $query->leftJoin('node__field_domain_access', 'nfda', 'n.nid = nfda.entity_id');
    if (!$options['force']) {
      $query->isNull('nfda.field_domain_access_target_id');
    }
    $query->distinct();
    $result = $query->execute()->fetchAll();

    if (empty($result)) {
      $this->logger()->warning(dt('No nodes to assign.'));
    }

    foreach ($result as $node) {
      $update_query = $this->db->insert('node__field_domain_access')
        ->fields([
          'bundle' => $node->type,
          'deleted' => 0,
          'entity_id' => $node->nid,
          'revision_id' => $node->vid,
          'langcode' => $node->langcode,
          'delta' => 0,
          'field_domain_access_target_id' => $default_domain,
        ]);
      $update_query->execute();
    }

    $this->logger()->success(dt(count($result) . ' nodes assigned.'));
  }

  /**
   * Assign menu items to the default domain.
   *
   * @command domain-sites:menu-link-content
   *
   * @option ids The IDs of the menus.
   * @option force Force assigning the default domain.
   *
   * @usage drush domain-sites:menu-link-content
   *   Assign menu items from the main menu to the default domain.
   * @usage drush domain-sites:menu-link-content --ids=footer
   *   Assign menu items from the footer menu to the default domain.
   * @usage drush domain-sites:menu-link-content --ids=main,footer
   *   Assign menu items from the main and footer menu to the default domain.
   * @usage drush domain-sites:menu-link-content --ids=footer --force
   *   Force assigning menu items from the footer menu to the default domain,
   *   even when they already have a domain assigned.
   *
   * @aliases domain-sites-menu-link-content
   */
  public function assignMenuLinks(
    array $options = ['ids' => 'main', 'force' => FALSE]
  ) {
    $default_domain = $this->domainStorage->loadDefaultId();

    if (!$default_domain) {
      throw new \Exception(dt('No default domain found.'));
    }

    if (empty($options['ids'])) {
      throw new \Exception(dt('No menu ID found.'));
    }

    $menus = explode(',', $options['ids']);

    // Get all menu links that don't have a domain yet.
    $query = $this->db->select('menu_link_content', 'mlc');
    $query->fields('mlc', ['id', 'revision_id', 'langcode', 'bundle']);
    $query->fields('mlcfda', ['entity_id', 'field_domain_access_target_id']);
    $query->leftJoin('menu_link_content__field_domain_access', 'mlcfda', 'mlc.id = mlcfda.entity_id');
    $query->condition('mlc.bundle', $menus, 'IN');
    if (!$options['force']) {
      $query->isNull('mlcfda.field_domain_access_target_id');
    }
    $query->distinct();
    $result = $query->execute()->fetchAll();

    if (empty($result)) {
      $this->logger()->warning(dt('No menu links to assign.'));
    }

    foreach ($result as $menu_link_content) {
      $update_query = $this->db->insert('menu_link_content__field_domain_access')
        ->fields([
          'bundle' => $menu_link_content->bundle,
          'deleted' => 0,
          'entity_id' => $menu_link_content->id,
          'revision_id' => $menu_link_content->revision_id,
          'langcode' => $menu_link_content->langcode,
          'delta' => 0,
          'field_domain_access_target_id' => $default_domain,
        ]);
      $update_query->execute();
    }

    $this->logger()->success(dt(count($result) . ' menu links assigned.'));
  }

}
