<?php

namespace Drupal\domain_sites\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain\DomainStorageInterface;
use Drupal\domain_sites\DomainSitesHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a back to main site link.
 *
 * @Block(
 *   id = "link_to_main_site",
 *   admin_label = @Translation("Link to main site block"),
 *   category = @Translation("Domain Sites"),
 * )
 */
class LinkToMainSite extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The domain storage.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The domain sites helper service.
   *
   * @var \Drupal\domain_sites\DomainSitesHelper
   */
  protected $domainSitesHelper;

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Constructs a LinkToMainSite instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\domain\DomainStorageInterface $domain_storage
   *   The domain storage.
   * @param \Drupal\domain_sites\DomainSitesHelper $domain_sites_helper
   *   The domain sites helper service.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DomainStorageInterface $domain_storage, DomainSitesHelper $domain_sites_helper, DomainNegotiatorInterface $domain_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->domainStorage = $domain_storage;
    $this->domainSitesHelper = $domain_sites_helper;
    $this->domainNegotiator = $domain_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('domain_sites.helper'),
      $container->get('domain.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'text' => $this->t('Back to main site'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\domain\Entity\Domain $domain */
    $domain = $this->domainStorage->loadDefaultDomain();

    // Check if we have a default domain.
    if (!empty($domain)) {
      $form['text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Text'),
        '#description' => $this->t('The text for the back link.'),
        '#required' => TRUE,
        '#default_value' => $this->configuration['text'],
      ];

      return $form;
    }
    else {
      return [
        '#markup' => $this->t('There is no domain configured.'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['text'] = $values['text'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    if (!$this->domainSitesHelper->backLinkEnabled()) {
      return AccessResult::forbidden('The back link is not enabled for this domain.');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\domain\Entity\Domain $domain */
    $domain = $this->domainStorage->loadDefaultDomain();

    // Check if we have a default domain.
    if (empty($domain)) {
      return NULL;
    }

    return [
      '#type' => 'link',
      '#title' => $this->configuration['text'],
      '#url' => $domain->toUrl('canonical'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.site']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $domain = $this->domainNegotiator->getActiveDomain();
    return Cache::mergeTags(parent::getCacheTags(), ['config:domain_sites.' . $domain->id() . '.settings']);
  }

}
