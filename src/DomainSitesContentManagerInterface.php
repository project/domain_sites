<?php

namespace Drupal\domain_sites;

/**
 * Content functions for domain sites.
 */
interface DomainSitesContentManagerInterface {

  /**
   * Create a frontpage for a given domain.
   *
   * @param string $type
   *   The node type.
   * @param string $title
   *   The node title.
   * @param string $domain_id
   *   The domain ID.
   *
   * @return int|null
   *   The created node ID.
   */
  public function createFrontpage(string $type, string $title, string $domain_id): ?int;

}
