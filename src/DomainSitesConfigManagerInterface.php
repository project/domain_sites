<?php

namespace Drupal\domain_sites;

/**
 * Config functions for domain sites.
 */
interface DomainSitesConfigManagerInterface {

  /**
   * Return the config name for a domain config type.
   *
   * @param string $type
   *   The config type.
   * @param string $domain_id
   *   The domain ID.
   *
   * @return string
   *   The config name.
   */
  public function getDomainConfigNameByType(string $type, string $domain_id): string;

  /**
   * Update domain specific config.
   *
   * @param string $config_name
   *   The config name to update.
   * @param array $values
   *   An array containing the key and values to be set.
   */
  public function updateDomainConfig(string $config_name, array $values): void;

}
