<?php

namespace Drupal\domain_sites;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Config functions for domain sites.
 */
class DomainSitesConfigManager implements DomainSitesConfigManagerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an DomainSitesConfigManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainConfigNameByType(string $type, string $domain_id): string {
    switch ($type) {
      case 'domain_config':
        return 'domain.config.' . $domain_id . '.system.site';

      case 'domain_access_logo':
        return 'domain_access_logo.settings';

      case 'domain_sites':
        return 'domain_sites.' . $domain_id . '.settings';

      default:
        return 'domain.record.' . $domain_id;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateDomainConfig(string $config_name, array $values): void {
    $config = $this->configFactory->getEditable($config_name);

    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();
  }

}
