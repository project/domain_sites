<?php

namespace Drupal\domain_sites\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\domain\DomainStorageInterface;

/**
 * Provides a controller for the domain sites.
 */
class DomainSitesController extends ControllerBase {

  /**
   * The entity storage.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DomainSitesControllerBase.
   *
   * @param \Drupal\domain\DomainStorageInterface $domain_storage
   *   The storage controller.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(DomainStorageInterface $domain_storage, EntityTypeManagerInterface $entity_type_manager) {
    $this->domainStorage = $domain_storage;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a list of domain sites.
   *
   * @return array
   *   A list of domain sites.
   */
  public function list() {
    $header = [
      'name' => $this->t('Name'),
      'hostname' => $this->t('Hostname'),
      'status' => $this->t('Status'),
      'operations' => $this->t('Operations'),
    ];
    $rows = [];

    $domains = $this->domainStorage->loadMultipleSorted();
    if (!empty($domains)) {
      /** @var \Drupal\domain\DomainInterface $domain */
      foreach ($domains as $domain) {
        $operations = [];
        if ($this->currentUser()->hasPermission('edit domain sites')) {
          $operations[] = [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('domain_sites.edit', ['domain_site' => $domain->id()]),
          ];
        }
        if ($this->currentUser()->hasPermission('translate domain sites')) {
          $operations[] = [
            'title' => $this->t('Translate'),
            'url' => Url::fromRoute('domain_sites.translate', ['domain_site' => $domain->id()]),
          ];
        }
        if ($this->domainStorage->loadDefaultId() !== $domain->id() && $this->currentUser()->hasPermission('delete domain sites')) {
          $operations[] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('domain_sites.delete', ['domain_site' => $domain->id()]),
          ];
        }

        $row = [
          'name' => $domain->label(),
          'hostname' => [
            'data' => [
              '#markup' => $domain->getLink(FALSE),
            ],
          ],
          'status' => $domain->status() ? $this->t('Active') : $this->t('Inactive'),
          'operations' => [
            'data' => [
              '#type' => 'dropbutton',
              '#dropbutton_type' => 'extrasmall',
              '#links' => $operations,
            ],
          ],
        ];

        if ($domain->isActive()) {
          $row['hostname']['data']['#prefix'] = '<strong>';
          $row['hostname']['data']['#suffix'] = '</strong>';
        }

        $rows[] = $row;
      }
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No domain sites found.'),
    ];
  }

}
