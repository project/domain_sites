<?php

namespace Drupal\domain_sites;

use Drupal\domain_access\DomainAccessManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Config functions for domain sites.
 */
class DomainSitesContentManager implements DomainSitesContentManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function createFrontpage(string $type, string $title, string $domain_id): ?int {
    try {
      $values = [
        'type' => $type,
        'title' => $title,
        'uid' => \Drupal::currentUser()->id(),
      ];

      $node = Node::create($values);
      $node->set(DomainAccessManagerInterface::DOMAIN_ACCESS_FIELD, [$domain_id]);
      $node->save();

      return $node->id();
    }
    catch (\Exception $exception) {
      watchdog_exception('debug', $exception);
      return NULL;
    }
  }

}
