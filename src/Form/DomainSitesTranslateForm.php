<?php

namespace Drupal\domain_sites\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\domain\DomainStorageInterface;
use Drupal\domain_sites\DomainSitesConfigManagerInterface;
use Drupal\file\FileStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to translate a domain site.
 */
class DomainSitesTranslateForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The domain storage.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The file storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The domain config manager.
   *
   * @var \Drupal\domain_sites\DomainSitesConfigManagerInterface
   */
  protected $domainSitesConfigManager;

  /**
   * Constructs a \Drupal\domain_sites\DomainSitesEditForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\domain\DomainStorageInterface $domain_storage
   *   The domain storage.
   * @param \Drupal\file\FileStorageInterface $file_storage
   *   The file storage.
   * @param \Drupal\domain_sites\DomainSitesConfigManagerInterface $domain_sites_config_manager
   *   The domain config manager.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, DomainStorageInterface $domain_storage, FileStorageInterface $file_storage, DomainSitesConfigManagerInterface $domain_sites_config_manager) {
    $this->moduleHandler = $moduleHandler;
    $this->domainStorage = $domain_storage;
    $this->fileStorage = $file_storage;
    $this->domainSitesConfigManager = $domain_sites_config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('domain_sites.config_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_sites_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $domain_site = NULL) {
    // Return when we have no domain site.
    if (empty($domain_site)) {
      return [];
    }

    // @todo Add this functionality.
    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Functionality not provided yet.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Add this functionality.
  }

}
