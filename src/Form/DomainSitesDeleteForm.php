<?php

namespace Drupal\domain_sites\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\domain\DomainStorageInterface;
use Drupal\domain_sites\DomainSitesConfigManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to delete a domain site.
 */
class DomainSitesDeleteForm extends ConfirmFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The domain storage.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The domain config manager.
   *
   * @var \Drupal\domain_sites\DomainSitesConfigManagerInterface
   */
  protected $domainSitesConfigManager;

  /**
   * ID of the domain site to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * Constructs a \Drupal\domain_sites\DomainSitesDeleteForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\domain\DomainStorageInterface $domain_storage
   *   The domain storage.
   * @param \Drupal\domain_sites\DomainSitesConfigManagerInterface $domain_sites_config_manager
   *   The domain config manager.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, DomainStorageInterface $domain_storage, DomainSitesConfigManagerInterface $domain_sites_config_manager) {
    $this->moduleHandler = $moduleHandler;
    $this->domainStorage = $domain_storage;
    $this->domainSitesConfigManager = $domain_sites_config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('domain_sites.config_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_sites_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $domain_site = NULL) {
    $this->id = $domain_site;

    if ($this->domainStorage->loadDefaultId() === $this->id) {
      return [
        '#markup' => $this->t('You can not delete the default site.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete domain record.
    $domain_record_config = $this->configFactory()->getEditable($this->domainSitesConfigManager->getDomainConfigNameByType('domain', $this->id));
    if (!empty($domain_record_config)) {
      $domain_record_config->delete();
    }

    // Delete domain config.
    $domain_config = $this->configFactory()->getEditable($this->domainSitesConfigManager->getDomainConfigNameByType('domain_config', $this->id));
    if (!empty($domain_config)) {
      $domain_config->delete();
    }

    // Delete domain access logo config.
    $domain_access_logo_config = $this->configFactory()->getEditable($this->domainSitesConfigManager->getDomainConfigNameByType('domain_access_logo', $this->id));
    $domain_access_logo_config_data = $domain_access_logo_config->getRawData();
    unset($domain_access_logo_config_data[$this->id]);
    $domain_access_logo_config->setData($domain_access_logo_config_data)->save();

    // Delete domain sites config.
    $domain_sites_config = $this->configFactory()->getEditable($this->domainSitesConfigManager->getDomainConfigNameByType('domain_sites', $this->id));
    if (!empty($domain_sites_config)) {
      $domain_sites_config->delete();
    }

    // Redirect to domain sites list.
    $form_state->setRedirect('domain_sites.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete this domain site?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('domain_sites.list');
  }

}
