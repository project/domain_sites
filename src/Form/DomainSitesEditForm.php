<?php

namespace Drupal\domain_sites\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\domain\DomainStorageInterface;
use Drupal\domain_sites\DomainSitesConfigManagerInterface;
use Drupal\file\FileStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to edit a domain site.
 */
class DomainSitesEditForm extends FormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The domain storage.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The file storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The domain config manager.
   *
   * @var \Drupal\domain_sites\DomainSitesConfigManagerInterface
   */
  protected $domainSitesConfigManager;

  /**
   * Constructs a \Drupal\domain_sites\DomainSitesEditForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\domain\DomainStorageInterface $domain_storage
   *   The domain storage.
   * @param \Drupal\file\FileStorageInterface $file_storage
   *   The file storage.
   * @param \Drupal\domain_sites\DomainSitesConfigManagerInterface $domain_sites_config_manager
   *   The domain config manager.
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, DomainStorageInterface $domain_storage, FileStorageInterface $file_storage, DomainSitesConfigManagerInterface $domain_sites_config_manager) {
    $this->moduleHandler = $moduleHandler;
    $this->domainStorage = $domain_storage;
    $this->fileStorage = $file_storage;
    $this->domainSitesConfigManager = $domain_sites_config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('domain_sites.config_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_sites_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $domain_site = NULL) {
    // Return when we have no domain site.
    if (empty($domain_site)) {
      return [];
    }

    // Return when we have no domain record.
    /** @var \Drupal\domain\DomainInterface $domain */
    $domain = $this->domainStorage->load($domain_site);
    if (empty($domain)) {
      return [];
    }

    $domain_sites_settings = $this->config('domain_sites.settings');
    $domain_site_specific_settings = $this->config($this->domainSitesConfigManager->getDomainConfigNameByType('domain_sites', $domain->id()));

    $form['domain_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Domain details'),
      '#open' => TRUE,
    ];

    $form['domain_information']['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#description' => $this->t('The human-readable name is shown in domain sites lists and is used as the title.'),
      '#required' => TRUE,
      '#default_value' => $domain->label(),
    ];

    $form['domain_information']['hostname'] = [
      '#title' => $this->t('Hostname'),
      '#type' => 'textfield',
      '#description' => $this->t('The canonical hostname, using the full <em>subdomain.example.com</em> format. Leave off the http:// and the trailing slash and do not include any paths.<br />If this domain uses a custom http(s) port, you should specify it here, e.g.: <em>subdomain.example.com:1234</em><br />The hostname may contain only lowercase alphanumeric characters, dots, dashes, and a colon (if using alternative ports).'),
      '#required' => TRUE,
      '#default_value' => $domain->getCanonical(),
    ];

    $form['domain_information']['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'source' => ['domain_information', 'name'],
        'exists' => [$this->domainStorage, 'load'],
      ],
      '#disabled' => TRUE,
      '#description' => $this->t('A unique machine-readable name for this domain site. It must only contain lowercase letters, numbers, and underscores.'),
      '#default_value' => $domain->id(),
    ];

    $form['domain_information']['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#options' => [
        1 => $this->t('Active'),
        0 => $this->t('Inactive'),
      ],
      '#default_value' => (int) $domain->status(),
      '#description' => $this->t('"Inactive" domain sites are only accessible to user roles with that assigned permission.'),
    ];

    $original_config_settings = $this->config('system.site');
    $domain_config_settings = $this->config($this->domainSitesConfigManager->getDomainConfigNameByType('domain_config', $domain->id()));

    $form['site_information'] = [
      '#type' => 'details',
      '#title' => $this->t('Site details'),
      '#open' => TRUE,
    ];
    $form['site_information']['site_slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slogan'),
      '#default_value' => $domain_config_settings->get('slogan') ?? $original_config_settings->get('slogan'),
      '#description' => $this->t("How this is used depends on your site's theme."),
      '#maxlength' => 255,
    ];
    $form['site_information']['site_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#default_value' => $domain_config_settings->get('mail') ?? $original_config_settings->get('mail'),
      '#description' => $this->t("The <em>From</em> address in automated emails sent during registration and new password requests, and other notifications. (Use an address ending in your site's domain to help prevent this email being flagged as spam.)"),
      '#required' => TRUE,
    ];

    $form['domain_extra'] = [
      '#type' => 'details',
      '#title' => $this->t('Domain extra'),
      '#open' => TRUE,
    ];

    if ($domain_sites_settings->get('allow_color_sets') === 1) {
      $form['domain_extra']['color_set'] = [
        '#title' => $this->t('Color set'),
        '#type' => 'select',
        '#description' => $this->t('The color set to use for the domain site. You can <a href=":add_color_set">add a color set</a>.', [
          ':add_color_set' => Url::fromRoute('<front>')->toString(),
        ]),
        '#options' => [
          '' => $this->t('Default'),
        ],
        '#default_value' => $domain_site_specific_settings->get('color_set') ?? '',
      ];
    }

    if ($this->moduleHandler->moduleExists('domain_access_logo') && $domain_sites_settings->get('allow_logo') === 1) {
      $domain_access_logo_config = $this->configFactory()->get($this->domainSitesConfigManager->getDomainConfigNameByType('domain_access_logo', $domain->id()));
      // List of logo extensions.
      $extensions = 'png gif jpg jpeg svg';

      $form['domain_extra']['logo'] = [
        '#title' => $this->t('Logo'),
        '#type' => 'managed_file',
        '#description' => $this->t('The logo for this domain site. <br>Allowed types: @extensions.', [
          '@extensions' => $extensions,
        ]),
        '#size' => 64,
        '#upload_validators' => [
          'file_validate_extensions' => [$extensions],
        ],
        '#upload_location' => 'public://files',
        '#default_value' => $domain_access_logo_config->get($domain->id()) ?? [],
      ];
    }

    if ($domain_sites_settings->get('allow_back_link') === 1) {
      $form['domain_extra']['back_link'] = [
        '#title' => $this->t('Back link'),
        '#type' => 'checkbox',
        '#description' => $this->t('Whether or not to show the back link to the main site.'),
        '#default_value' => $domain_site_specific_settings->get('back_link') ?? TRUE,
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Domain.
    $domain_values = [
      'hostname' => $form_state->getValue('hostname'),
      'name' => $form_state->getValue('name'),
      'status' => $form_state->getValue('status'),
    ];
    $domain_config_name = $this->domainSitesConfigManager->getDomainConfigNameByType('domain', $form_state->getValue('id'));
    $this->domainSitesConfigManager->updateDomainConfig($domain_config_name, $domain_values);

    // Domain config system site.
    $default_system_site = $this->config('system.site');
    $domain_config_values = [
      'name' => $form_state->getValue('name'),
      'mail' => $form_state->getValue('site_mail'),
      'slogan' => $form_state->getValue('site_slogan'),
      'page.403' => $default_system_site->get('page.403'),
      'page.404' => $default_system_site->get('page.404'),
      'admin_compact_mode' => $default_system_site->get('admin_compact_mode'),
      'weight_selected_max' => $default_system_site->get('weight_selected_max'),
      'default_langcode' => $default_system_site->get('default_langcode'),
      'mail_notification' => $default_system_site->get('mail_notification'),
    ];
    $domain_config_config_name = $this->domainSitesConfigManager->getDomainConfigNameByType('domain_config', $form_state->getValue('id'));
    $this->domainSitesConfigManager->updateDomainConfig($domain_config_config_name, $domain_config_values);

    // Domain Access Logo.
    if ($this->moduleHandler->moduleExists('domain_access_logo')) {
      $domain_access_logo_config_name = $this->domainSitesConfigManager->getDomainConfigNameByType('domain_access_logo', $form_state->getValue('id'));
      $domain_access_logo_config = $this->config($domain_access_logo_config_name);

      $original_logo = $domain_access_logo_config->get($form_state->getValue('id'));
      $logo = $form_state->getValue('logo');

      // Delete original logo when new logo is not the same and save new logo.
      if ($original_logo != $logo) {
        if (!empty($original_logo)) {
          $original_file = $this->fileStorage->load($original_logo[0]);
          if (!empty($original_file)) {
            $original_file->delete();
          }
        }

        // Set new file permanent.
        if (!empty($logo)) {
          $file = $this->fileStorage->load($logo[0]);
          if (!empty($file)) {
            $file->setPermanent();
            $file->save();
          }
        }

        $domain_access_logo_values = [
          $form_state->getValue('id') => $logo,
        ];
        $this->domainSitesConfigManager->updateDomainConfig($domain_access_logo_config_name, $domain_access_logo_values);
      }
    }

    // Domain sites.
    $domain_sites_values = [
      'color_set' => $form_state->getValue('color_set'),
      'back_link' => $form_state->getValue('back_link'),
    ];
    $domain_sites_config_name = $this->domainSitesConfigManager->getDomainConfigNameByType('domain_sites', $form_state->getValue('id'));
    $this->domainSitesConfigManager->updateDomainConfig($domain_sites_config_name, $domain_sites_values);

    // Redirect to domain sites list.
    $form_state->setRedirect('domain_sites.list');
  }

}
