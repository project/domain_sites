<?php

namespace Drupal\domain_sites\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to configure settings for the domain sites module.
 */
class DomainSitesSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\domain_sites\DomainSitesSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_sites_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['domain_sites.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('domain_sites.settings');

    $form['content_type'] = [
      '#title' => $this->t('Content type'),
      '#type' => 'select',
      '#description' => $this->t('The content type used to create the frontpage for the sites.'),
      '#options' => $this->getContentTypesAsOptions(),
      '#default_value' => $config->get('content_type') ?? 'page',
    ];

    $form['allow_back_link'] = [
      '#title' => $this->t('Allow back link'),
      '#type' => 'checkbox',
      '#description' => $this->t('Whether or not to allow the back link to the main site to be configured per domain site.'),
      '#default_value' => $config->get('allow_back_link') ?? 1,
    ];

    // $form['allow_color_sets'] = [
    // '#title' => $this->t('Allow color sets'),
    // '#type' => 'checkbox',
    // '#description' => $this->t('Whether or not to allow color sets to
    // be configured per domain site.'),
    // '#default_value' => $config->get('allow_color_sets') ?? 1,
    // ];
    if ($this->moduleHandler->moduleExists('domain_access_logo')) {
      $form['allow_logo'] = [
        '#title' => $this->t('Allow logo'),
        '#type' => 'checkbox',
        '#description' => $this->t('Whether or not to allow the logo to be configured per domain site.'),
        '#default_value' => $config->get('allow_logo') ?? 0,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('domain_sites.settings');
    $config
      ->set('content_type', $form_state->getValue('content_type'))
      ->set('allow_back_link', $form_state->getValue('allow_back_link'))
    // ->set('allow_color_sets', $form_state->getValue('allow_color_sets') ?? 0)
      ->set('allow_logo', $form_state->getValue('allow_logo') ?? 0)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Provides the content types as options list.
   *
   * @return array
   *   An array containing the content types.
   */
  protected function getContentTypesAsOptions(): array {
    $content_types_options_list = [];
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($content_types as $content_type) {
      $content_types_options_list[$content_type->id()] = $content_type->label();
    }
    return $content_types_options_list;
  }

}
