<?php

namespace Drupal\domain_sites;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\node\Entity\Node;

/**
 * Helper functions for domain sites.
 */
class DomainSitesHelper {

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an DomainSitesHelper object.
   *
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(DomainNegotiatorInterface $domain_negotiator, ConfigFactoryInterface $config_factory) {
    $this->domainNegotiator = $domain_negotiator;
    $this->configFactory = $config_factory;
  }

  /**
   * Check if the given node is a frontpage of a domain site.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The current node.
   *
   * @return bool
   *   Whether the current node is a frontpage of a domain site.
   */
  public function isFrontPage(Node $node): bool {
    $domain = $this->domainNegotiator->getActiveDomain();
    if (empty($domain)) {
      return FALSE;
    }

    // Check if node is frontpage for active domain.
    $active_domain_config = $this->configFactory->get('domain.config.' . $domain->id() . '.system.site');
    if (!empty($active_domain_config) && '/node/' . $node->id() == $active_domain_config->get('page.front')) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns the frontpage content type.
   *
   * @return string
   *   The content type.
   */
  public function frontPageContentType(): string {
    $domain_sites_settings = $this->configFactory->get('domain_sites.settings');
    if (!empty($domain_sites_settings) && !empty($domain_sites_settings->get('content_type'))) {
      return $domain_sites_settings->get('content_type');
    }

    return 'page';
  }

  /**
   * Check if the back link is enabled for a domain site.
   *
   * @return bool
   *   Whether the back link is enabled.
   */
  public function backLinkEnabled(): bool {
    $domain = $this->domainNegotiator->getActiveDomain();
    if (empty($domain)) {
      return FALSE;
    }

    // Check if back link is enabled for active domain.
    $active_domain_sites_settings = $this->configFactory->get('domain_sites.' . $domain->id() . '.settings');
    if (!empty($active_domain_sites_settings) && $active_domain_sites_settings->get('back_link')) {
      return TRUE;
    }

    return FALSE;
  }

}
