<?php

/**
 * @file
 * Primary module hooks for domain sites module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function domain_sites_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the domain_sites module.
    case 'help.page.domain_sites':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Domain Sites provides an easy configuration for setting up (sub)sites based on domain modules.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_page_attachments().
 */
function domain_sites_page_attachments(array &$page) {
  // Add Admin Toolbar overrides when logged in.
  if (\Drupal::currentUser()->isAuthenticated()) {
    $page['#attached']['library'][] = 'domain_sites/toolbar';
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function domain_sites_preprocess_page(&$variables) {
  $default_domain = TRUE;

  if (Drupal::moduleHandler()->moduleExists('domain')) {
    /** @var \Drupal\domain\Entity\Domain $domain */
    $domain = \Drupal::service('domain.negotiator')->getActiveDomain();
    if (!empty($domain)) {
      $default_domain = $domain->isDefault();
    }
  }

  $variables['default_domain'] = $default_domain;
}
